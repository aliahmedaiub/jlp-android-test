package com.aliahmed.core_ui

sealed class NavigationItem(val route: String, val icon: Int, val title: String) {

}
