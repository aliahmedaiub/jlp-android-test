package com.aliahmed.data.network

import com.aliahmed.data.model.BaseResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("catalog/products/search/keyword")
    suspend fun getDishwasher(
        @Query("key") key: String = BaseURL.apiKey,
        @Query("q") location: String = "dishwasher"
    ): Result<BaseResponse>

}