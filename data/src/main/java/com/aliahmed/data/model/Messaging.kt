package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Messaging(
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String
)