package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Value(
    @SerializedName("max")
    val max: String,
    @SerializedName("min")
    val min: String
)