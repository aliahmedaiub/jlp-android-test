package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class VariantPriceRange(
    @SerializedName("display")
    val display: Display,
    @SerializedName("for")
    val forX: String,
    @SerializedName("reductionHistory")
    val reductionHistory: List<ReductionHistory>,
    @SerializedName("value")
    val value: Value
)