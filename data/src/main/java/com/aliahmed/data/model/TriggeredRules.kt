package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class TriggeredRules(
    @SerializedName("dynamic")
    val _dynamic: String,
    @SerializedName("seo")
    val seo: String
)