package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Service(
    @SerializedName("automaticallyIncluded")
    val automaticallyIncluded: Boolean,
    @SerializedName("title")
    val title: String,
    @SerializedName("__typename")
    val typename: String
)