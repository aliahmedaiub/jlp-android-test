package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class BaseResponse(
    @SerializedName("banners")
    val banners: Banners,
    @SerializedName("baseFacetId")
    val baseFacetId: String,
    @SerializedName("crumbs")
    val crumbs: List<Crumb>,
    @SerializedName("dynamicBannerId")
    val dynamicBannerId: String,
    @SerializedName("endecaCanonical")
    val endecaCanonical: String,
    @SerializedName("facets")
    val facets: List<Facet>,
    @SerializedName("isPersonalised")
    val isPersonalised: Boolean,
    @SerializedName("pageInformation")
    val pageInformation: PageInformation,
    @SerializedName("pagesAvailable")
    val pagesAvailable: Int,
    @SerializedName("products")
    val products: List<Product>,
    @SerializedName("redirectType")
    val redirectType: String,
    @SerializedName("results")
    val results: Int,
    @SerializedName("seoBannerId")
    val seoBannerId: String,
    @SerializedName("showInStockOnly")
    val showInStockOnly: Boolean,
    @SerializedName("triggeredRules")
    val triggeredRules: TriggeredRules
)