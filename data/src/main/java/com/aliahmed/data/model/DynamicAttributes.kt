package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class DynamicAttributes(
    @SerializedName("adjustable")
    val adjustable: String,
    @SerializedName("amperage")
    val amperage: String,
    @SerializedName("annualrunningcost")
    val annualrunningcost: String,
    @SerializedName("augmentedrealityimageavailable")
    val augmentedrealityimageavailable: String,
    @SerializedName("autodose")
    val autodose: String,
    @SerializedName("automaticloadadjustment")
    val automaticloadadjustment: String,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("cablelength")
    val cablelength: String,
    @SerializedName("childlock")
    val childlock: String,
    @SerializedName("colour")
    val colour: String,
    @SerializedName("combinedaperturedimensions")
    val combinedaperturedimensions: String,
    @SerializedName("countryoforigin")
    val countryoforigin: String,
    @SerializedName("crediteligibilitystatus")
    val crediteligibilitystatus: String,
    @SerializedName("creditofferingids")
    val creditofferingids: String,
    @SerializedName("cutlerybasket")
    val cutlerybasket: String,
    @SerializedName("cycledurationatratedcapacityfortheecocycle")
    val cycledurationatratedcapacityfortheecocycle: String,
    @SerializedName("delicatewash")
    val delicatewash: String,
    @SerializedName("digitaldisplay")
    val digitaldisplay: String,
    @SerializedName("dimensions")
    val dimensions: String,
    @SerializedName("dishwashersize")
    val dishwashersize: String,
    @SerializedName("drainagefacilityavailable")
    val drainagefacilityavailable: String,
    @SerializedName("dryingperformance")
    val dryingperformance: String,
    @SerializedName("dryingsystem")
    val dryingsystem: String,
    @SerializedName("energyratingoverall")
    val energyratingoverall: String,
    @SerializedName("fittingsincluded")
    val fittingsincluded: String,
    @SerializedName("floodprotection")
    val floodprotection: String,
    @SerializedName("guarantee")
    val guarantee: String,
    @SerializedName("homeappliancefeatures")
    val homeappliancefeatures: String,
    @SerializedName("homeappliancetype")
    val homeappliancetype: String,
    @SerializedName("homearea")
    val homearea: String,
    @SerializedName("installationrequired")
    val installationrequired: String,
    @SerializedName("integratedorfreestanding")
    val integratedorfreestanding: String,
    @SerializedName("international")
    val international: String,
    @SerializedName("invertermotor")
    val invertermotor: String,
    @SerializedName("manufacturerpartnumbermpn")
    val manufacturerpartnumbermpn: String,
    @SerializedName("modelnamenumber")
    val modelnamenumber: String,
    @SerializedName("noiselevel")
    val noiselevel: String,
    @SerializedName("noiselevelrating")
    val noiselevelrating: String,
    @SerializedName("noofprograms")
    val noofprograms: String,
    @SerializedName("onlineexclusive")
    val onlineexclusive: String,
    @SerializedName("placesettings")
    val placesettings: String,
    @SerializedName("producttype1")
    val producttype1: String,
    @SerializedName("programsequenceindicator")
    val programsequenceindicator: String,
    @SerializedName("quickwash")
    val quickwash: String,
    @SerializedName("quickwashcycletime")
    val quickwashcycletime: String,
    @SerializedName("quietmark")
    val quietmark: String,
    @SerializedName("range")
    val range: String,
    @SerializedName("rinseaidindicator")
    val rinseaidindicator: String,
    @SerializedName("saltlevelindicator")
    val saltlevelindicator: String,
    @SerializedName("slimdepth")
    val slimdepth: String,
    @SerializedName("smarthometype")
    val smarthometype: String,
    @SerializedName("smarttechnology")
    val smarttechnology: String,
    @SerializedName("timerdelay")
    val timerdelay: String,
    @SerializedName("timeremainingindicator")
    val timeremainingindicator: String,
    @SerializedName("truecolour")
    val truecolour: String,
    @SerializedName("watersupplyfilltype")
    val watersupplyfilltype: String,
    @SerializedName("weight")
    val weight: String,
    @SerializedName("weightedenergyconsumptionper100cyclesforecocycle")
    val weightedenergyconsumptionper100cyclesforecocycle: String,
    @SerializedName("weightedwaterconsumptionfortheecocycle")
    val weightedwaterconsumptionfortheecocycle: String,
    @SerializedName("widthbuiltinovens")
    val widthbuiltinovens: String
)