package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Display(
    @SerializedName("max")
    val max: String,
    @SerializedName("min")
    val min: String
)