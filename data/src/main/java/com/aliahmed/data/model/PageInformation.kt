package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class PageInformation(
    @SerializedName("description")
    val description: String,
    @SerializedName("heading")
    val heading: String,
    @SerializedName("noFollow")
    val noFollow: Boolean,
    @SerializedName("noIndex")
    val noIndex: Boolean,
    @SerializedName("title")
    val title: String
)