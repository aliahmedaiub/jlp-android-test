package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class HiddenAttribute(
    @SerializedName("displayName")
    val displayName: String,
    @SerializedName("key")
    val key: String,
    @SerializedName("values")
    val values: List<String>
)