package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class ColorSwatche(
    @SerializedName("basicColor")
    val basicColor: String,
    @SerializedName("color")
    val color: String,
    @SerializedName("colorSwatchUrl")
    val colorSwatchUrl: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("imageUrl")
    val imageUrl: String,
    @SerializedName("isAvailable")
    val isAvailable: Boolean,
    @SerializedName("isColorOfDefaultVariant")
    val isColorOfDefaultVariant: Boolean,
    @SerializedName("skuId")
    val skuId: String
)