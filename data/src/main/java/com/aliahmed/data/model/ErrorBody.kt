package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class ErrorBody(
    @SerializedName("code")
    val code: Int,
    @SerializedName("message")
    val message: String
)