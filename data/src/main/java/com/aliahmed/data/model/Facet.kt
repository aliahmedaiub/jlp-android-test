package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Facet(
    @SerializedName("details")
    val details: List<Detail>,
    @SerializedName("dimensionName")
    val dimensionName: String,
    @SerializedName("filterTypes")
    val filterTypes: List<String>,
    @SerializedName("name")
    val name: String,
    @SerializedName("tooltip")
    val tooltip: String
)