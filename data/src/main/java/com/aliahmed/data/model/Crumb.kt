package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Crumb(
    @SerializedName("clickable")
    val clickable: String,
    @SerializedName("displayName")
    val displayName: String,
    @SerializedName("item")
    val item: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("url")
    val url: String
)