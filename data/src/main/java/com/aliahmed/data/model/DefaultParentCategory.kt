package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class DefaultParentCategory(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)