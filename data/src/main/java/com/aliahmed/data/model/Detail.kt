package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Detail(
    @SerializedName("color")
    val color: String,
    @SerializedName("colorSwatchUrl")
    val colorSwatchUrl: String,
    @SerializedName("facetId")
    val facetId: String,
    @SerializedName("isSelected")
    val isSelected: String,
    @SerializedName("label")
    val label: String,
    @SerializedName("qty")
    val qty: String,
    @SerializedName("trackingId")
    val trackingId: String
)