package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("ageRestriction")
    val ageRestriction: Int,
    @SerializedName("alternativeImageUrls")
    val alternativeImageUrls: List<String>,
    @SerializedName("attributes")
    val attributes: List<Attribute>,
    @SerializedName("averageRating")
    val averageRating: Double,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("colorSwatches")
    val colorSwatches: List<ColorSwatche>,
    @SerializedName("compare")
    val compare: Boolean,
    @SerializedName("defaultParentCategory")
    val defaultParentCategory: DefaultParentCategory,
    @SerializedName("defaultSkuId")
    val defaultSkuId: String,
    @SerializedName("defaultVariantId")
    val defaultVariantId: String,
    @SerializedName("displaySpecialOffer")
    val displaySpecialOffer: String,
    @SerializedName("dynamicAttributes")
    val dynamicAttributes: DynamicAttributes,
    @SerializedName("emailMeWhenAvailable")
    val emailMeWhenAvailable: Boolean,
    @SerializedName("fabric")
    val fabric: String,
    @SerializedName("fabricByLength")
    val fabricByLength: Boolean,
    @SerializedName("futureRelease")
    val futureRelease: Boolean,
    @SerializedName("hiddenAttributes")
    val hiddenAttributes: List<HiddenAttribute>,
    @SerializedName("image")
    val image: String,
    @SerializedName("isAvailableToOrder")
    val isAvailableToOrder: Boolean,
    @SerializedName("isBundle")
    val isBundle: Boolean,
    @SerializedName("isInStoreOnly")
    val isInStoreOnly: Boolean,
    @SerializedName("isMadeToMeasure")
    val isMadeToMeasure: Boolean,
    @SerializedName("isProductSet")
    val isProductSet: Boolean,
    @SerializedName("messaging")
    val messaging: List<Messaging>,
    @SerializedName("multiSku")
    val multiSku: Boolean,
    @SerializedName("nonPromoMessage")
    val nonPromoMessage: String,
    @SerializedName("outOfStock")
    val outOfStock: Boolean,
    @SerializedName("permanentOos")
    val permanentOos: Boolean,
    @SerializedName("productId")
    val productId: String,
    @SerializedName("reviews")
    val reviews: Int,
    @SerializedName("services")
    val services: List<Service>,
    @SerializedName("swatchAvailable")
    val swatchAvailable: Boolean,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("variantPriceRange")
    val variantPriceRange: VariantPriceRange
)