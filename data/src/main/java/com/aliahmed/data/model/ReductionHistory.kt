package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class ReductionHistory(
    @SerializedName("chronology")
    val chronology: Int,
    @SerializedName("display")
    val display: Display
)