package com.aliahmed.data.model


import com.google.gson.annotations.SerializedName

data class Banners(
    @SerializedName("blockBannerId")
    val blockBannerId: String,
    @SerializedName("seoBannerId")
    val seoBannerId: String,
    @SerializedName("topBannerId")
    val topBannerId: String
)