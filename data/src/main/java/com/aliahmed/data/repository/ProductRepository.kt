package com.aliahmed.data.repository

import com.aliahmed.data.model.BaseResponse
import com.aliahmed.data.network.ApiService
import javax.inject.Inject

interface ProductRepository  {
    suspend fun getDishwasher() : Result<BaseResponse>
}

class ProductRepositoryImpl @Inject constructor(private val apiService: ApiService) : ProductRepository {

    override suspend fun getDishwasher(): Result<BaseResponse> {
        return apiService.getDishwasher()
    }

}