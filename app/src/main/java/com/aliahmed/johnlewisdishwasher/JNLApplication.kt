package com.aliahmed.johnlewisdishwasher

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class JNLApplication : Application() {
}